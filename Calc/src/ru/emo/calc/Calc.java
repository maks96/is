package ru.emo.calc;

public class Calc {


    /**
     * Базовый класс калькулятор, позволяющий представлять действия над числами.
     * Параметр x - 1 число.
     * Параметр y - 2 число.
     *
     * @author Евстигнеев Максим.
     */
    // Сложение 2 чисел.
    public static double add(double x, double y) {
        return x + y;
    }

    // Вычитание 2 чисел.
    public static double sub(double x, double y) {
        return x - y;
    }

    // Умножение 2 чисел.
    public static double multi(double x, double y) {
        return x * y;
    }

    // Вещественное деление чисел с обработкой исключения( деление на ноль).
    public static double div(double x, double y) {
        if (y == 0) {
            throw new ArithmeticException("Деление на 0");
        }
        return x / y;
    }

    // Целочисленное деление чисел с обработкой исключения( деление на ноль).
    public static int div2(double x, double y) {

        if (y == 0) {
            throw new ArithmeticException("Деление на 0");
        }
        return (int) (x / y);

    }

    // Выделение остатка от деления.
    public static int ost(double x, double y) {
        return (int) (x % y);

    }


    //Возведение числа в отрицательную степень
    public static double powTemp(double x, double y) {
        double resTemp = x;
        for (double i = 1; i < y; i++) {
            resTemp *= x;
        }
        return resTemp;
    }


    //Возведение числа в положительную и нулевую степень
    public static double pow(double x, double y) {
        double pow2 = x;
        if (y > 0) {
            pow2 = powTemp(x, y);
        }

        if (y == 0) {
            pow2 = 1;
        }

        if (y < 0) {

            pow2 = 1 / powTemp(x, y);
        }
        return pow2;
    }



}