package ru.emo.calcaddition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    public final static String FIRST_TERM = "first";
    public final static String SECOND_TERM = "second";

    private EditText term1;
    private EditText term2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        term1 = (EditText) findViewById(R.id.termFirst);
        term2 = (EditText) findViewById(R.id.termSecond);

        findViewById(R.id.buttonAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (term1.getText().toString().equals("")) {
                    term1.setError(getString(R.string.error));
                    return;
                } else if (term1.getText().toString().equals("-")) {
                    term1.setError(getString(R.string.error));
                    return;
                }
                if (term2.getText().toString().equals("")) {
                    term2.setError(getString(R.string.error));
                    return;
                } else if (term2.getText().toString().equals("-")) {
                    term2.setError(getString(R.string.error));
                    return;
                }

                int first = Integer.valueOf(term1.getText().toString());
                int second = Integer.valueOf(term2.getText().toString());

                Intent intent = new Intent(MainActivity.this, ResultActivity.class);
                intent.putExtra(FIRST_TERM, first);
                intent.putExtra(SECOND_TERM, second);
                startActivity(intent);
            }
        });
    }
}
