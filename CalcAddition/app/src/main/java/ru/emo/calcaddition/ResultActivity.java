package ru.emo.calcaddition;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import org.w3c.dom.Text;

public class ResultActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        Intent intent = getIntent();

        int first = intent.getIntExtra(MainActivity.FIRST_TERM, 0);
        int second = intent.getIntExtra(MainActivity.SECOND_TERM, 0);
        int result =  Addition(first, second);

        TextView message = (TextView) findViewById(R.id.result);
        String mess = String.valueOf(first);
        mess += " + ";
        mess += (second < 0) ? "(" + second + ")": second;
        mess += " = ";
        mess += result;

        message.setTextSize(22);
        message.setText(mess);
    }

    public Integer Addition(int a, int b){
        return a + b;
    }
}
