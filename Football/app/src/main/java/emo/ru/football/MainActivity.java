package emo.ru.football;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private Integer countOfTeamRussian = 0;
    private Integer countOfTeamCanada = 0;
    TextView teamRussian;
    TextView teamCanada;
    TextView message;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        teamRussian = (TextView) findViewById(R.id.teamRussian);
        teamCanada = (TextView) findViewById(R.id.teamCanada);
        message = (TextView) findViewById(R.id.message);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        countOfTeamRussian = savedInstanceState.getInt("countTeamRussian");
        countOfTeamCanada = savedInstanceState.getInt("countTeamCanada");
        teamRussian.setText(countOfTeamRussian.toString());
        teamCanada.setText(countOfTeamCanada.toString());
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("countTeamRussian", countOfTeamRussian);
        outState.putInt("countTeamCanada", countOfTeamCanada);
    }

    public void onClickAddCountTeamRussian(View view){
        countOfTeamRussian++;
        if (countOfTeamRussian <= 5){
            teamRussian.setText(countOfTeamRussian.toString());
        } else {
            message.setText(getString(R.string.Russia) + " " + getString(R.string.max_count));
        }
    }

    public void onClickAddCountTeamCanada(View view){
        countOfTeamCanada++;
        if (countOfTeamCanada <= 5){
            teamCanada.setText(countOfTeamCanada.toString());
        } else {
            message.setText(getString(R.string.Canada) + " " + getString(R.string.max_count));
        }
    }

    public void onClickResetCount(View view){
        countOfTeamRussian = 0;
        countOfTeamCanada = 0;
        teamRussian.setText(countOfTeamRussian.toString());
        teamCanada.setText(countOfTeamCanada.toString());
        message.setText("");
    }
}
